This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## React example app
I'm creating this app to learn the basics of React + Redux combination in a real example, based on the experience I got from posts on the web, and some tutorials I will later link here.

Feel free to clone it and modify it, if it can help you to learn too.

If you want to run it, first install the dependencies with `npm install` and then you can run the app locally with `npm start`
